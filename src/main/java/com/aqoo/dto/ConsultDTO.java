package com.aqoo.dto;

import com.aqoo.entity.Consulting;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @ClassName ConsultDTO
 * @Description
 * @Date 2023/6/28 17:31
 * @Version 1.0
 */
@Data
@Accessors(chain = true)
public class ConsultDTO extends Consulting {

    /**
     * 学生姓名
     */
    private String studentName;

    /**
     * 学生学号
     */
    private String studentNumber;

    /**
     * 咨询师姓名
     */
    private String consultName;

    /**
     * 心理助理姓名
     */
    private String assistantName;

    /**
     * 是否结案  1代表结案
     */
    private Integer isEnd;

    /**
     * 时间段
     */
    private String period;

    /**
     * 星期几
     */
    private String dayOfWeek;
}
