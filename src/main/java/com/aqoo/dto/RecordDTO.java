package com.aqoo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @ClassName RecordDTO
 * @Description
 * @Date 2023/6/29 17:04
 * @Version 1.0
 */
@Data
@Accessors(chain = true)
public class RecordDTO {
    /**
     * 学生姓名
     */
    private String studentName;

    /**
     * 咨询师姓名
     */
    private String consultantName;

    /**
     * 咨询状态
     */
    private String state;

    /**
     * 咨询日期
     */
    private String consultDate;

    /**
     * 咨询记录
     */
    private String resultRecord;
}
