package com.aqoo.dto;

import lombok.Data;

@Data
public class UserInfoDTO {
    // 这三个字段是属于你的用户表的 子字段
    private Integer id;
    private String username;
    private Integer userType;
}
