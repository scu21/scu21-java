package com.aqoo.dto;

import com.aqoo.entity.Visit;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @ClassName VisitDTO
 * @Description
 * @Date 2023/6/27 16:47
 * @Version 1.0
 */
@Data
@Accessors(chain = true)
public class VisitDTO extends Visit {

    /**
     * 学生姓名
     */
    private String studentName;

    /**
     * 学生学号
     */
    private String studentNumber;

    /**
     *初访员姓名
     */
    private String interviewerName; 

    /**
     * 时间
     */
    private String visitTime;

}
