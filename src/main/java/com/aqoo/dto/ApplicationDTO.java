package com.aqoo.dto;

import com.aqoo.entity.Application;
import com.aqoo.entity.User;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @ClassName ApplicationDTO
 * @Description
 * @Date 2023/6/26 9:43
 * @Version 1.0
 */
@Data
@Accessors(chain = true)
public class ApplicationDTO {

    /**
     * 对应申请表id
     */
    private Integer applicationId;

    /**
     * 申请学生姓名
     */
    private String studentName;

    /**
     * 申请学生学号(也就是学生登录账号)
     */
    private String studentNumber;

    /**
     * 申请学生id
     */
    private Integer studentId;

    /**
     * 预约的日期
     */
    private String applyDate;

    /**
     * 预约时间段
     */
    private String period;

    /**
     * 初访员姓名
     */
    private String interviewerName;

    /**
     * 问卷分数
     */
    private Integer score;

    /**
     * 审核状态
     */
    private Integer status;

    /**
     * 初访状态
     */
    private Integer interviewState;

}
