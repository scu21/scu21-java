package com.aqoo.dto;

import com.aqoo.entity.Schedule;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @ClassName ScheduleDTO
 * @Description
 * @Date 2023/6/28 16:10
 * @Version 1.0
 */
@Data
@Accessors(chain = true)
public class ScheduleDTO {

    /**
     * 时间段标识
     */
    private Integer periodNumber;

    /**
     * 当前用户类型
     */
    private String type;

    /**
     * 当前用户姓名
     */
    private String name;


    private Integer weekDay;
}
