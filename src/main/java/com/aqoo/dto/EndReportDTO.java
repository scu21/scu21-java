package com.aqoo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @ClassName EndReportDTO
 * @Description
 * @Date 2023/7/2 11:10
 * @Version 1.0
 */
@Data
@Accessors(chain = true)
public class EndReportDTO {

    private String loginNumber;

    private String studentName;

    private String sex;

    private String department;

    private String phoneNumber;

    private String crisisLevel;

    private Integer count;

    private String selfComment;
}
