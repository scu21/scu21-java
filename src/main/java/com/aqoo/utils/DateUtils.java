package com.aqoo.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @ClassName DateUtils
 * @Description 根据日期计算星期几
 * @Date 2023/6/25 18:17
 * @Version 1.0
 */
public class DateUtils {
    public static Integer transferDateTo(String date) {
        int dayOfWeek = -1;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date d = format.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(d);
            dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
            dayOfWeek = dayOfWeek == 0 ? 7 : dayOfWeek;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dayOfWeek;
    }
}
