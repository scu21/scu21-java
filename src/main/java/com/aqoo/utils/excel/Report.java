package com.aqoo.utils.excel;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @ClassName Report
 * @Description
 * @Date 2023/7/1 19:33
 * @Version 1.0
 */
@Data
@Accessors(chain = true)
public class Report {

    @ExcelExport("工号")
    private String loginNumber;

    @ExcelExport("姓名")
    private String consultantName;

    @ExcelExport("咨询人次")
    private Integer userCount;

    @ExcelExport("时间总数(时间段次数)")
    private Integer timeCount;

}
