package com.aqoo.utils;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;


/**
 * @ClassName CodeGenerator
 * @Description
 * @Date 2023/6/23 20:13
 * @Version 1.0
 */
public class CodeGenerator {
    public static void main(String[] args) {
        FastAutoGenerator.create("jdbc:mysql://localhost:3306/suncaper?useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=UTC", "root", "mysql123")
                .globalConfig(builder -> {
                    builder.author("lpx")
                            .outputDir(System.getProperty("user.dir")+"/src/main/java");
                })
                .packageConfig(builder -> {
                    builder.parent("com.aqoo");
                })
                .templateEngine(new FreemarkerTemplateEngine())
                .execute();
    }
}
