package com.aqoo.utils;

import com.aqoo.entity.User;
import com.aqoo.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @ClassName UserUtils
 * @Description
 * @Date 2023/6/28 11:14
 * @Version 1.0
 */
@Component
public class UserUtils {

    private static UserUtils userUtils;

    @Resource
    private UserService userService;

    @PostConstruct
    public void init() {
        userUtils = this;
        userUtils.userService = this.userService;
    }

    public static String getNameById(Integer id) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getId, id);
        User user = userUtils.userService.getOne(wrapper);
        return user.getUsername();
    }

    public static String getLoginNumberById(Integer id) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getId, id);
        User user = userUtils.userService.getOne(wrapper);
        return user.getLoginNumber();
    }

    public static Integer getUserTypeById(Integer id) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getId, id);
        User user = userUtils.userService.getOne(wrapper);
        return user.getUserType();
    }
}
