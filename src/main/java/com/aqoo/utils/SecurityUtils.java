package com.aqoo.utils;

import com.aqoo.entity.User;
import com.aqoo.dto.UserInfoDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SecurityUtils {
    /**
     * 获取当前用户
     *
     * @return
     */
    public static User getCurrentUserInfo() {
        User userInfo = SessionUtils.getCurrentUserInfo();
        //模拟登录
        if (userInfo == null) {
            userInfo = new User();
            userInfo.setLoginNumber("模拟");
        }

        return userInfo;
    }

    public static UserInfoDTO getUserInfo() {
        // 从session获取当前登录用户
        User userInfo = SessionUtils.getCurrentUserInfo();
        // 1: 登录过了  能获取到值
        // 2: 没登陆  得到的肯定是null
        UserInfoDTO userInfoDTO = new UserInfoDTO();
            //模拟登录  没登陆假装你登录了（这个代码一定只能在开发中用）
            if (userInfo == null) {
                userInfo = new User();
                userInfo.setLoginNumber("111");
                userInfoDTO.setId(1);
                userInfoDTO.setUsername("111");
                userInfoDTO.setUserType(0);
        }else{
            userInfoDTO.setId(userInfo.getId());
            userInfoDTO.setUsername(userInfo.getUsername());
            userInfoDTO.setUserType(userInfo.getUserType());
        }

        return userInfoDTO;
    }
}
