package com.aqoo.utils;

import com.aqoo.entity.Period;
import com.aqoo.service.PeriodService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @ClassName PeriodUtils
 * @Description 根据时间段标识periodNumber查找时间段
 * @Date 2023/6/28 11:02
 * @Version 1.0
 */
@Component
public class PeriodUtils {

    private static PeriodUtils periodUtils;

    @Resource
    private PeriodService periodService;

    @PostConstruct
    public void init() {
        periodUtils = this;
        periodUtils.periodService = this.periodService;
    }

    public static String getPeriodByNumber(Integer periodNumber) {
        LambdaQueryWrapper<Period> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Period::getNumber, periodNumber);
        Period period = periodUtils.periodService.getOne(wrapper);
        return period.getTimePeriod();
    }

    public static Integer getDayOfWeekByNumber(Integer periodNumber) {
        LambdaQueryWrapper<Period> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Period::getNumber, periodNumber);
        Period period = periodUtils.periodService.getOne(wrapper);
        return period.getDayWeek();
    }
}
