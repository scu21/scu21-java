package com.aqoo.common;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName Result
 * @Description 通用返回结果，服务端响应的所有数据都封装成此对象
 * @Date 2023/6/24 9:28
 * @Version 1.0
 */
@Data
public class Result<T> {


    /**
     * 编码
     */
    private Integer code;

    /**
     * 错误信息
     */
    private String msg;


    /**
     * 响应数据
     */
    private T data;

    /**
     * 动态数据
     */
    private Map map = new HashMap();

    public static <T> Result<T> error(String msg) {
        Result result = new Result();
        result.code = 0;
        result.msg = msg;
        return result;
    }

    public static <T> Result<T> success(T object) {
        Result result = new Result<T>();
        result.code = 1;
        result.data = object;
        return result;
    }

    public Result<T> add(String key, Object value) {
        this.map.put(key, value);
        return this;
    }
}
