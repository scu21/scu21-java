package com.aqoo.controller;

import com.aqoo.common.Result;
import com.aqoo.dto.RecordDTO;
import com.aqoo.entity.Record;
import com.aqoo.service.RecordService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
@RestController
@RequestMapping("/api/record")
public class RecordController {

    @Autowired
    private RecordService recordService;

    /**
     * 在提交record时，提交之后，新建一条一周后的record记录
     * 还需要将 consulting的count字段加一
     * @param record
     * @return
     */
    @PostMapping("/commit")
    public Result commit(@RequestBody Record record) {
        recordService.updateById(record);
        try {
            recordService.saveNextRecord(record);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Result.success(null);
    }

    /**
     * 中心管理员 咨询师 获取 某个咨询的所有记录
     * 按照创建时间从早到晚查
     * @param applicationId
     * @return
     */
    @GetMapping("/getRecord")
    public Result getRecord(Integer applicationId) {
        List<RecordDTO> recordDTOList = recordService.getRecord(applicationId);
        return Result.success(recordDTOList);
    }

    /**
     * 咨询师在填写record时用于回显使用
     */
    @GetMapping("/getMyLastRecord")
    public Result getMyRecord(Integer applicationId) {
        LambdaQueryWrapper<Record> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Record::getApplicationId, applicationId).isNull(Record::getConsultRecord);
        Record record = recordService.getOne(wrapper);
        return Result.success(record);
    }
}
