package com.aqoo.controller;

import com.aqoo.common.Result;
import com.aqoo.entity.Questionnaire;
import com.aqoo.service.QuestionnaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 获取问卷
 * @author lpx
 * @since 2023-06-23
 */
@RestController
@RequestMapping("/api/questionnaire")
public class QuestionnaireController {

    @Autowired
    private QuestionnaireService questionnaireService;

    @GetMapping
    public Result<List> getQuestion() {
        List<Questionnaire> questions = questionnaireService.list();
        return Result.success(questions);
    }
}
