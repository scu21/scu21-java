package com.aqoo.controller;

import com.aqoo.common.Result;
import com.aqoo.dto.EndReportDTO;
import com.aqoo.entity.Application;
import com.aqoo.entity.Consulting;
import com.aqoo.entity.Record;
import com.aqoo.entity.User;
import com.aqoo.entity.Visit;
import com.aqoo.service.*;
import com.aqoo.utils.excel.ExcelUtils;
import com.aqoo.utils.excel.Report;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import fr.opensagres.xdocreport.template.formatter.FieldsMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @ClassName CommonController
 * @Description
 * @Date 2023/6/30 9:49
 * @Version 1.0
 */
@RestController
@RequestMapping("/api/common")
public class CommonController {

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private VisitService visitService;

    @Autowired
    private UserService userService;

    @Autowired
    private ConsultingService consultingService;

    @Autowired
    private RecordService recordService;

    @GetMapping("/getState")
    public Result getState(HttpSession session) {

        Integer[] appStates = new Integer[5];
        Arrays.fill(appStates, 0);

        Integer userId = (Integer) session.getAttribute("userId");
        Integer applicationId = (Integer) session.getAttribute("applicationId");

        //当前session中有学生id  流程id
        //获取session中的 applicatioId 如果不为空 说明申请过 appStates[0] = 1
        if (applicationId == null) {
            return Result.success(appStates);
        }
        appStates[0] = 1;
        //根据applicationId获取state 0-审核中  1-审核通过  2-审核不通过
        //state为0时，正在审核 appStates[1] = 0
        //state为1时，审核通过 appStates[1] = 1
        //state为2时，审核不通过 appStates[1] = 2
        Application application = applicationService.getById(applicationId);
        appStates[1] = application.getState();

        //appStates[1] = 0 或者 2 说明后续流程还走不到
        if (appStates[1] == 0 || appStates[1] == 2) {
            return Result.success(appStates);
        }
        //appStates[1]为1时，说明审核通过，审核通过后，进入初访
        //如果没有初访表则 直接为0
        //查看初访表看是否已经初访   如果没初访直接返回  如果已经初访则 appStates[2] = 1
        LambdaQueryWrapper<Visit> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Visit::getApplicationId, applicationId);
        Visit visit = visitService.getOne(wrapper);

        if (visit == null || visit.getState() == 0) {
            return Result.success(appStates);
        }
        appStates[2] = 1;

        //查看初访表的is_arranged字段 为0代表心理助理还没约
        if (visit.getIsArranged() == 0) {
            return Result.success(appStates);
        }

        //不为0说明已经安排了 处于咨询中
        appStates[3] = 1;

        //看是否结案
        LambdaQueryWrapper<Consulting> wrapper1 = new LambdaQueryWrapper<>();
        wrapper1.eq(Consulting::getApplicationId, applicationId);
        Consulting consulting = consultingService.getOne(wrapper1);

        if (consulting == null || consulting.getLastDate() == null) {
            return Result.success(appStates);
        }
        appStates[4] = 1;
        return Result.success(appStates);

        //appStates[0] 为0说明从来没申请过，为1说明申请过
        //appStates[1] 为0正在审核 为1审核通过 为2审核不通过
        //appStates[2] 为0没有初访 为1已初访
        //appStates[3] 为0没有初访安排  为1已经初访安排（心理助理填了表）
        //appStates[4] 为0说明没有结案 为1说明已结案
    }

    @PostMapping("/import")
    public Result upload(@RequestPart("file") MultipartFile file) throws Exception {
        List<User> users = ExcelUtils.readMultipartFile(file, User.class);
        List<User> userList = userService.list();

        for (int i = 0; i < users.size(); i++) {
            String loginNumber = users.get(i).getLoginNumber();
            users.get(i).setPassword(loginNumber);
            for (int j = 0; j < userList.size(); j++) {
                if (loginNumber.equals(userList.get(j).getLoginNumber())) {
                    users.remove(i);
                }
            }
        }

        userService.saveBatch(users);
        return Result.success("成功导入");
    }

    @PostMapping("exportTemplate")
    public void exportTemplate(HttpServletResponse response) {
        ExcelUtils.exportTemplate(response, "用户信息表", User.class, true);
    }


    /**
     * 导出每个咨询师的咨询人次和时间总数
     */
    @PostMapping("/exportConsulting")
    public void exportConsulting(HttpServletResponse response) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getUserType, 3);
        List<User> consultantList = userService.list(wrapper);

        List<Report> reportList = new ArrayList<>();

        consultantList.stream().forEach(consultant -> {
            Report report = new Report();
            report.setLoginNumber(consultant.getLoginNumber());
            report.setConsultantName(consultant.getUsername());

            LambdaQueryWrapper<Consulting> wrapper1 = new LambdaQueryWrapper<>();
            wrapper1.eq(Consulting::getConsultantId, consultant.getId());
            Integer count = Math.toIntExact(consultingService.count(wrapper1));
            report.setUserCount(count);

            List<Consulting> consultingList = consultingService.list(wrapper1);
            int sum = consultingList.stream().mapToInt(Consulting::getCount).sum();
            report.setTimeCount(sum);

            reportList.add(report);

        });

        ExcelUtils.export(response, "咨询汇总", reportList, Report.class);
    }

    @GetMapping("/exportDoc")
    public void exportDoc(Integer id, HttpServletResponse response) {
        //结案的
        LambdaQueryWrapper<Consulting> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Consulting::getId, id).isNotNull(Consulting::getLastDate);
        Consulting consulting = consultingService.getOne(wrapper);

        EndReportDTO endReportDTO = new EndReportDTO();

        //填充学生信息
        LambdaQueryWrapper<User> wrapper1 = new LambdaQueryWrapper<>();
        wrapper1.eq(User::getId, consulting.getStudentId());
        User user = userService.getOne(wrapper1);

        endReportDTO.setLoginNumber(user.getLoginNumber()).setStudentName(user.getUsername())
                .setDepartment(user.getDepartment()).setPhoneNumber(user.getPhoneNumber())
                .setSex(user.getGender());

        //填充咨询效果自评
        endReportDTO.setSelfComment(consulting.getSelfComment());

        //咨询总次数
        Integer applicationId = consulting.getApplicationId();
        LambdaQueryWrapper<Record> wrapper2 = new LambdaQueryWrapper<>();
        wrapper2.eq(Record::getApplicationId, applicationId).isNotNull(Record::getConsultRecord);
        Integer count = Math.toIntExact(recordService.count(wrapper2));

        endReportDTO.setCount(count);

        //危机等级
        LambdaQueryWrapper<Visit> wrapper3 = new LambdaQueryWrapper<>();
        wrapper3.eq(Visit::getApplicationId, applicationId);
        Visit visit = visitService.getOne(wrapper3);
        String crisisLevel = "";
        Integer intCrisisLevel = visit.getCrisisLevel();
        if (intCrisisLevel == 0) {
            crisisLevel = "轻度";
        } else if (intCrisisLevel == 1) {
            crisisLevel = "中度";
        } else {
            crisisLevel = "重度";
        }
        endReportDTO.setCrisisLevel(crisisLevel);

        try {
            generateWord(endReportDTO, response);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XDocReportException e) {
            e.printStackTrace();
        }

    }

    public void generateWord(EndReportDTO endReportDTO, HttpServletResponse response) throws IOException, XDocReportException {
        //获取Word模板，模板存放路径在项目的resources目录下
        InputStream ins = this.getClass().getResourceAsStream("/1.docx");
        //注册xdocreport实例并加载FreeMarker模板引擎
        IXDocReport report = XDocReportRegistry.getRegistry().loadReport(ins,
                TemplateEngineKind.Freemarker);
        //创建xdocreport上下文对象
        IContext context = report.createContext();

        context.put("endReportDTO", endReportDTO);

        //创建字段元数据
        FieldsMetadata fm = report.createFieldsMetadata();
        //Word模板中的表格数据对应的集合类型
        fm.load("goods", EndReportDTO.class, true);

        //浏览器端下载
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/msword");
        String fileName = "结案报告表.docx";
        response.setHeader("Content-Disposition", "attachment;filename="
                .concat(String.valueOf(URLEncoder.encode(fileName, "UTF-8"))));
        report.process(context, response.getOutputStream());
    }
}
