package com.aqoo.controller;



import com.aqoo.common.JsonResponse;
import com.aqoo.utils.SecurityUtils;
import com.aqoo.dto.UserInfoDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @GetMapping("/userInfo")
    public JsonResponse<UserInfoDTO> getUserInfo() {
        return JsonResponse.success(SecurityUtils.getUserInfo());
    }
}
