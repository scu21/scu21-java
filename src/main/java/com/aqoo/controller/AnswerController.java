package com.aqoo.controller;

import com.aqoo.common.Result;
import com.aqoo.entity.Answer;
import com.aqoo.entity.Application;
import com.aqoo.entity.Questionnaire;
import com.aqoo.service.AnswerService;
import com.aqoo.service.ApplicationService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import javax.servlet.http.HttpSession;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * 保存问卷答案   获取问卷答案
 *
 * @author lpx
 * @since 2023-06-23
 */
@RestController
@RequestMapping("/api/answer")
public class AnswerController {

    @Autowired
    private AnswerService answerService;

    @Autowired
    private ApplicationService applicationService;

    @PostMapping("/commit")
    public Result commitAnswer(@RequestBody List<Questionnaire> list, HttpSession session) {

        Integer userId = (Integer) session.getAttribute("userId");
        Integer applicationId = answerService.saveAnswerAndScore(list, userId);
        //将流程id存入session
        session.setAttribute("applicationId", applicationId);
        return Result.success(applicationId);
    }

    @GetMapping("/getAnswer")
    public Result<List<Answer>> getAnswer(Integer applicationId, HttpSession session) {

        LambdaQueryWrapper<Answer> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Answer::getApplicationId, applicationId);
            List<Answer> answers = answerService.list(wrapper);
        return Result.success(answers);

    }

    @GetMapping("getAnswerLists")
    public Result<List<Application>> getAnswerLists(HttpSession session) {

        Integer userId = (Integer) session.getAttribute("userId");
        LambdaQueryWrapper<Application> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Application::getStudentId, userId);
        List<Application> list = applicationService.list(wrapper);
        return Result.success(list);

    }

}
