package com.aqoo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
@RestController
@RequestMapping("/api/period")
public class PeriodController {

}
