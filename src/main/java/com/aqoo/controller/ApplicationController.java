package com.aqoo.controller;

import com.aqoo.common.Result;
import com.aqoo.dto.ApplicationDTO;
import com.aqoo.service.ApplicationService;
import javax.servlet.http.HttpSession;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
@RestController
@RequestMapping("/api/application")
public class ApplicationController {

    @Autowired
    private ApplicationService applicationService;

    /**
     * 学生提交初访预约申请表（其实是修改某些字段，因为在提交问卷答案时已经新建了记录）
     *
     * @param map
     * @param session
     * @return
     */
    @PostMapping("/commit")
    public Result commit(@RequestBody Map map, HttpSession session) {
        Integer studentId = (Integer) session.getAttribute("userId");
        Integer applicationId = (Integer) session.getAttribute("applicationId");

        if (applicationId == null)
            return Result.success(false);
        String date = String.valueOf(map.get("date"));
        String period = String.valueOf(map.get("period"));
        Integer interviewerId = (Integer) map.get("id");

        boolean isNotDuplicated = applicationService.commit(applicationId, studentId, date, period, interviewerId);
        return Result.success(isNotDuplicated);
    }

    /**
     * 中心管理员获取所有的已经约过初访员的申请表
     * 初访员获取预约了自己的申请信息
     * 学生获取自己的所有申请信息
     * @return
     */
    @PostMapping("/getAll")
    public Result getAll(@RequestBody Map map, HttpSession session) {

        Integer userId = (Integer) session.getAttribute("userId");

        Integer pageNo = (Integer) map.get("pageNo");
        Integer pageSize = (Integer) map.get("pageSize");
        String sName = (String) map.get("sName");
        String iName = (String) map.get("iName");

        //所有的申请表
        List<ApplicationDTO> applicationDTOList = applicationService.getAll(userId);
        //拿出满足条件的
        List<ApplicationDTO> collect = applicationDTOList.stream().filter(dto ->
                        (sName.length() != 0 && dto.getStudentName().contains(sName)) ||
                        (iName.length() != 0 && dto.getInterviewerName().contains(iName)) ||
                        (iName.length() == 0 && sName.length() == 0)
        ).skip((pageNo-1)*pageSize).limit(pageSize).collect(Collectors.toList());
        return Result.success(collect);
    }

    /**
     * 中心管理员进行审核
     * @param applicationId
     * @param state
     * @return
     */
    @GetMapping("/check")
    public Result updateState(Integer applicationId, Integer state) {
        applicationService.updateState(applicationId, state);
        return Result.success(null);
    }

//    /**
//     * 初访员获取预约了自己的申请信息
//     * 学生获取自己的所有申请信息
//     * @param session
//     * @return
//     */
//    @GetMapping("/getMy")
//    public Result getMy(HttpSession session) {
//        Integer userId = (Integer) session.getAttribute("userId");
//        List<ApplicationDTO> applicationDTOList = applicationService.getMy(userId);
//        return Result.success(applicationDTOList);
//    }


}
