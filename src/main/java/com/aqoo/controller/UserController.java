package com.aqoo.controller;

import com.aqoo.common.Result;
import com.aqoo.entity.Application;
import com.aqoo.entity.User;
import com.aqoo.service.ApplicationService;
import com.aqoo.service.UserService;
import com.aqoo.utils.SessionUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationService applicationService;

    @PostMapping("/login")
    public Result login(@RequestBody Map<String, String> map, HttpSession session) {

        String loginNumber = map.get("account");
        String password = map.get("password");

        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getIsDeleted, 0).eq(User::getLoginNumber, loginNumber).eq(User::getPassword, password);
        User user = userService.getOne(wrapper);

        if (user != null) {
            SessionUtils.saveCurrentUserInfo(user);
            session.setAttribute("userId", user.getId());
            session.setAttribute("userName", user.getUsername());
            session.setAttribute("userType", user.getUserType());

            //如果是学生，登录就获取最近一次的applicationId
            if (user.getUserType() == 0) {
                LambdaQueryWrapper<Application> wrapper1 = new LambdaQueryWrapper<>();
                wrapper1.eq(Application::getStudentId, user.getId()).orderByDesc(Application::getCreateTime)
                        .last("limit 1");
                Application application = applicationService.getOne(wrapper1);
                if (application != null) {
                    session.setAttribute("applicationId", application.getId());
                }
            }

        }

        return Result.success(user);
    }

    @PostMapping("/getInterviewers")
    public Result getInterviewers(@RequestBody Map<String, String> map) {
        String date = map.get("date");
        String period = map.get("period");

        List<User> interviewers = userService.getFreeInterviewers(date, period);
        return Result.success(interviewers);
    }

    @PostMapping("/getAllUsers")
    public Result getAllUsers(@RequestBody Map<String, Integer> map) {

        Integer pageNo = map.get("pageNo");
        Integer pageSize = map.get("pageSize");
        Integer role = map.get("role");
        Page pageInfo = new Page<>(pageNo, pageSize);
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getIsDeleted, 0).eq(role != null, User::getUserType, role).orderByAsc(User::getUserType);
        userService.page(pageInfo, wrapper);
        return Result.success(pageInfo);
    }

    @GetMapping("/deleteById")
    public Result deleteById(Integer id) {
        LambdaUpdateWrapper<User> wrapper = new LambdaUpdateWrapper();
        wrapper.eq(User::getId, id).set(User::getIsDeleted, 1);
        userService.update(wrapper);
        return Result.success(null);
    }

    @PostMapping("/modify")
    public Result modify(@RequestBody User user) {
        userService.updateById(user);
        return Result.success(null);
    }

    @PostMapping("/getFreeConsultant")
    public Result getFreeConsultant(@RequestBody Map map) {
        String date = (String) map.get("date");
        String period = (String) map.get("period");
        List<User> freeConsultants = userService.getFreeConsultant(date, period);
        return Result.success(freeConsultants);
    }

    @GetMapping("/whoami")
    public Result whoAmI(HttpSession session) {
        Integer userId = (Integer) session.getAttribute("userId");
        User user = userService.getById(userId);
        return Result.success(user);
    }

    @GetMapping("/quit")
    public Result quit(HttpSession session) {
        session.invalidate();
        return Result.success(null);
    }

}
