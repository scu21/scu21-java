package com.aqoo.controller;

import com.aqoo.VO.ConsultVO;
import com.aqoo.common.Result;
import com.aqoo.dto.ConsultDTO;
import com.aqoo.entity.Consulting;
import com.aqoo.service.ConsultingService;
import com.aqoo.utils.PeriodUtils;
import com.aqoo.utils.UserUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
@RestController
@RequestMapping("/api/consulting")
public class ConsultingController {

    @Autowired
    private ConsultingService consultingService;

    @PostMapping("/commit")
    public Result commit(@RequestBody ConsultVO consultVO, HttpSession session) {
        Integer assistantId = (Integer) session.getAttribute("userId");
        consultVO.setAssistantId(assistantId);
        consultingService.commit(consultVO);
        return Result.success(null);
    }

    @PostMapping("/getConsult")
    public Result getConsult(@RequestBody Map<String, Integer> map, HttpSession session) {

        Integer pageNo = map.get("pageNo");
        Integer pageSize = map.get("pageSize");
        Integer isEnd = map.get("isEnd");

        Integer userId = (Integer) session.getAttribute("userId");
        List<ConsultDTO> consultDTOList = consultingService.getConsult(userId);

        Stream<ConsultDTO> stream = consultDTOList.stream();
        if (isEnd != null && (isEnd == 0 || isEnd == 1)) {
            stream = stream.filter(c -> c.getIsEnd() == isEnd);
        }

        List<ConsultDTO> list = stream.skip((pageNo - 1) * pageSize).limit(pageSize).collect(Collectors.toList());

        return Result.success(list);
    }

    /**
     * 心理助理获取自己负责的部分信息
     *
     * @return
     */
    @GetMapping("getMy")
    public Result getMy(Integer applicationId) {
        LambdaQueryWrapper<Consulting> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Consulting::getApplicationId, applicationId);
        Consulting consulting = consultingService.getOne(wrapper);
        if (consulting == null) {
            return Result.success(null);
        }
        Map<String, String> map = new HashMap<>();
        map.put("firstDate", consulting.getFirstDate());
        map.put("period", PeriodUtils.getPeriodByNumber(consulting.getPeriodNumber()));
        map.put("consultPlace", consulting.getConsultPlace());
        map.put("consultant", UserUtils.getNameById(consulting.getConsultantId()));
        return Result.success(map);

    }

    /**
     * 咨询师获取自己的
     *
     * @param session
     * @return
     */
    @GetMapping("/getMyConsulting")
    public Result getMyConsulting(HttpSession session) {
        Integer consultantId = (Integer) session.getAttribute("userId");
        List<ConsultDTO> consultDTOList = consultingService.getMyConsulting(consultantId);
        return Result.success(consultDTOList);
    }

    /**
     * 结案
     *
     * @param consulting
     * @return
     */
    @PostMapping("/end")
    public Result end(@RequestBody Consulting consulting) {
        consultingService.end(consulting);
        return Result.success(null);
    }

    /**
     * 咨询师申请追加
     * @param id
     * @return
     */
    @GetMapping("/applyAdd")
    public Result applyAdd(Integer id) {
        Consulting consulting = consultingService.getById(id);
        consulting.setIsAdd(1);
        consultingService.updateById(consulting);
        return Result.success(null);
    }

    @GetMapping("check")
    public Result check(Integer id, Integer isAdd) {
        Consulting consulting = consultingService.getById(id);
        consulting.setIsAdd(isAdd);
        consultingService.updateById(consulting);
        return Result.success(null);
    }

    /**
     * 根据applicationId获取consulting记录
     */
    @GetMapping("getOneConsulting")
    public Result getConsultingByApplicationId(Integer applicationId) {
        LambdaQueryWrapper<Consulting> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Consulting::getApplicationId, applicationId);
        Consulting consulting = consultingService.getOne(wrapper);
        return Result.success(consulting);
    }
}
