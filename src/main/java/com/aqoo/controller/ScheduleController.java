package com.aqoo.controller;

import com.aqoo.common.Result;
import com.aqoo.dto.ScheduleDTO;
import com.aqoo.entity.User;
import com.aqoo.service.RecordService;
import com.aqoo.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
@RestController
@RequestMapping("/api/schedule")
public class ScheduleController {

    @Autowired
    private ScheduleService scheduleService;

    @GetMapping("/getSchedule")
    public Result getSchedule(HttpSession session) {
        Integer userId = (Integer) session.getAttribute("userId");
        List<ScheduleDTO> scheduleDTOList = scheduleService.getSchedule(userId);
        return Result.success(scheduleDTOList);
    }
}
