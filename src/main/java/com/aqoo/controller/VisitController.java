package com.aqoo.controller;

import com.aqoo.common.Result;
import com.aqoo.dto.VisitDTO;
import com.aqoo.entity.Visit;
import com.aqoo.service.VisitService;
import com.aqoo.utils.UserUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
@RestController
@RequestMapping("/api/visit")
public class VisitController {

    @Autowired
    private VisitService visitService;

    /**
     * 初访后，初访员填写初访表
     *
     * @param visit
     * @return
     */
    @PostMapping("/commit")
    public Result commit(@RequestBody Visit visit, HttpSession session) {
        Integer interviewerId = (Integer) session.getAttribute("userId");
        visit.setInterviewerId(interviewerId);
        visitService.commit(visit);
        return Result.success(null);
    }

    /**
     * 心理助理获取所有的已初访的信息
     *
     * @return
     */
    @PostMapping("/getAllVisit")
    public Result getAllVisit(@RequestBody Map map, HttpSession session) {

        Integer userId = (Integer) session.getAttribute("userId");

        Integer pageNo = (Integer) map.get("pageNo");
        Integer pageSize = (Integer) map.get("pageSize");
        List<VisitDTO> visitDTOList = visitService.getAllVisit(userId);
        List<VisitDTO> pageDTO = visitDTOList.stream().skip((pageNo - 1) * pageSize).limit(pageSize).collect(Collectors.toList());
        return Result.success(pageDTO);
    }

    @GetMapping("/getOne")
    public Result getOne(Integer applicationId) {
        LambdaQueryWrapper<Visit> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Visit::getApplicationId, applicationId);
        Visit visit = visitService.getOne(wrapper);
        VisitDTO dto = new VisitDTO();
        BeanUtils.copyProperties(visit, dto);
        dto.setInterviewerName(UserUtils.getNameById(dto.getInterviewerId()));
        return Result.success(dto);
    }
}
