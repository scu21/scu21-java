package com.aqoo.VO;

import com.aqoo.entity.Consulting;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @ClassName ConsultVO
 * @Description
 * @Date 2023/6/28 14:39
 * @Version 1.0
 */
@Data
@Accessors(chain = true)
public class ConsultVO extends Consulting {
    private String period;
}
