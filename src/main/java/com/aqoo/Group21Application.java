package com.aqoo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.aqoo.mapper")
public class Group21Application {

    public static void main(String[] args) {
        SpringApplication.run(Group21Application.class, args);
    }

}
