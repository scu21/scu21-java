package com.aqoo.service;

import com.aqoo.dto.ApplicationDTO;
import com.aqoo.entity.Application;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
public interface ApplicationService extends IService<Application> {

    boolean commit(Integer applicationId, Integer studentId, String date, String period, Integer interviewerId);

    List<ApplicationDTO> getAll(Integer userId);

    void updateState(Integer applicationId, Integer state);

//    List<ApplicationDTO> getMy(Integer userId);
}
