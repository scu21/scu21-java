package com.aqoo.service;

import com.aqoo.dto.RecordDTO;
import com.aqoo.entity.Record;
import com.baomidou.mybatisplus.extension.service.IService;

import java.text.ParseException;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
public interface RecordService extends IService<Record> {

    List<RecordDTO> getRecord(Integer applicationId);

    void saveNextRecord(Record record) throws ParseException;
}
