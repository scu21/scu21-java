package com.aqoo.service;

import com.aqoo.entity.Questionnaire;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
public interface QuestionnaireService extends IService<Questionnaire> {


}
