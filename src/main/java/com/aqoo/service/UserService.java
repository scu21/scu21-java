package com.aqoo.service;

import com.aqoo.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *
 * @author lpx
 * @since 2023-06-23
 */
public interface UserService extends IService<User> {

    List<User> getFreeInterviewers(String date, String period);

    List<User> getFreeConsultant(String date, String period);
}
