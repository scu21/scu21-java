package com.aqoo.service.impl;

import com.aqoo.entity.Answer;
import com.aqoo.entity.Application;
import com.aqoo.entity.Questionnaire;
import com.aqoo.mapper.AnswerMapper;
import com.aqoo.service.AnswerService;
import com.aqoo.service.ApplicationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
@Service
public class AnswerServiceImpl extends ServiceImpl<AnswerMapper, Answer> implements AnswerService {

    @Autowired
    private ApplicationService applicationService;

    /**
     *
     * @param list
     * @param userId
     * @return 返回申请表id 即流程id
     */
    @Override
    public Integer saveAnswerAndScore(List<Questionnaire> list, Integer userId) {
        //获取问卷得分
        int sum = list.stream().mapToInt(Questionnaire::getQuestionScore).sum();
        //看有无高危问题
        boolean is_dangerous = list.stream().anyMatch(questionnaire -> questionnaire.getIsDangerous() == 1);
        //存入申请表中
        Application application = new Application();
        application.setScore(sum).setStudentId(userId).setIsDangerous(is_dangerous == true ? 1 : 0);
        applicationService.save(application);

        //新建答案列表
        List<Answer> answers = new ArrayList<>();
        //存答案表
        //将问卷中问题的信息赋值给答案
        list.stream().forEach(questionnaire -> {
            Answer answer = new Answer().setStudentId(userId).setApplicationId(application.getId()).
                    setQuestionScore(questionnaire.getQuestionScore()).setQuestionnaireId(questionnaire.getId())
                    .setIsDangerous(questionnaire.getIsDangerous());
            answers.add(answer);
        });
        saveBatch(answers);

        return application.getId();
    }
}
