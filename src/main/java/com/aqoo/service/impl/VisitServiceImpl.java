package com.aqoo.service.impl;

import com.aqoo.dto.VisitDTO;
import com.aqoo.entity.Schedule;
import com.aqoo.entity.Visit;
import com.aqoo.mapper.VisitMapper;
import com.aqoo.service.ScheduleService;
import com.aqoo.service.UserService;
import com.aqoo.service.VisitService;
import com.aqoo.utils.PeriodUtils;
import com.aqoo.utils.UserUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
@Service
public class VisitServiceImpl extends ServiceImpl<VisitMapper, Visit> implements VisitService {

    @Autowired
    private UserService userService;

    @Autowired
    private ScheduleService scheduleService;

    @Override
    public void commit(Visit visit) {
        visit.setState(1);
        //根据visit获取
        LambdaQueryWrapper<Visit> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Visit::getApplicationId, visit.getApplicationId()).eq(Visit::getStudentId, visit.getStudentId());
        Visit one = getOne(wrapper);
        visit.setId(one.getId());
        updateById(visit);

        //初访员填写初访表后，排班表对应时段设为 free
        Integer interviewerId = visit.getInterviewerId();
        Integer periodNumber = visit.getPeriodNumber();
        LambdaUpdateWrapper<Schedule> wrapper2 = new LambdaUpdateWrapper<>();
        wrapper2.eq(Schedule::getUserId, interviewerId).eq(Schedule::getPeriodNumber, periodNumber)
                .set(Schedule::getIsFree, 1);
        scheduleService.update(wrapper2);

    }

    @Override
    public List<VisitDTO> getAllVisit(Integer userId) {


        LambdaQueryWrapper<Visit> wrapper = new LambdaQueryWrapper<>();
        //查到所有已初访的初访表
        wrapper.eq(Visit::getState, 1);

        //看是什么用户
        Integer userType = UserUtils.getUserTypeById(userId);
        if (userType == 0) {
            //学生-> 查自己的
            wrapper.eq(Visit::getStudentId, userId);

        } else if (userType == 1) {
            //初访员 -> 查自己的
            wrapper.eq(Visit::getInterviewerId, userId);

        }

        //心理助理和中心管理员查所有的

        List<Visit> visitList = list(wrapper);

        //将查到的Visit包装为 VisitDTO
        List<VisitDTO> visitDTOList = new ArrayList<>();
        visitList.stream().forEach(visit -> {
            VisitDTO dto = new VisitDTO();
            BeanUtils.copyProperties(visit, dto);
            //学生姓名 学生学号
            Integer studentId = dto.getStudentId();
            dto.setStudentName(UserUtils.getNameById(dto.getStudentId()))
                    .setStudentNumber(UserUtils.getLoginNumberById(dto.getStudentId()));

            //初访员姓名
            dto.setInterviewerName(UserUtils.getNameById(dto.getInterviewerId()));

            //时间段
            dto.setVisitTime(PeriodUtils.getPeriodByNumber(dto.getPeriodNumber()));
            visitDTOList.add(dto);

        });

        return visitDTOList;
    }
}
