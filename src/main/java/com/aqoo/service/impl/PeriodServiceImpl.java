package com.aqoo.service.impl;

import com.aqoo.entity.Period;
import com.aqoo.mapper.PeriodMapper;
import com.aqoo.service.PeriodService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
@Service
public class PeriodServiceImpl extends ServiceImpl<PeriodMapper, Period> implements PeriodService {

    @Override
    public int getPeriodNumber(String period, int dayOfWeek) {
        LambdaQueryWrapper<Period> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Period::getDayWeek, dayOfWeek).eq(Period::getTimePeriod, period);
        Period period1 = getOne(wrapper);
        return period1 == null ? 0 : period1.getNumber() ;
    }
}
