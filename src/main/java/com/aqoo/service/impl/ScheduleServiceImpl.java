package com.aqoo.service.impl;

import com.aqoo.dto.ScheduleDTO;
import com.aqoo.entity.Application;
import com.aqoo.entity.Schedule;
import com.aqoo.entity.User;
import com.aqoo.mapper.ScheduleMapper;
import com.aqoo.service.ApplicationService;
import com.aqoo.service.PeriodService;
import com.aqoo.service.ScheduleService;
import com.aqoo.service.UserService;
import com.aqoo.utils.DateUtils;
import com.aqoo.utils.PeriodUtils;
import com.aqoo.utils.UserUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
@Service
public class ScheduleServiceImpl extends ServiceImpl<ScheduleMapper, Schedule> implements ScheduleService {

    @Autowired
    private ScheduleService scheduleService;

    @Override
    public List<ScheduleDTO> getSchedule(Integer userId) {
        Integer userType = UserUtils.getUserTypeById(userId);
        LambdaQueryWrapper<Schedule> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Schedule::getUserId, userId);
        //如果是中心管理员 查询所有  因此不需要wrapper条件
        if (userType == 4) {
            wrapper.clear();
        }
        List<Schedule> scheduleList = scheduleService.list(wrapper);

        //将包装为scheduleDTO
        List<ScheduleDTO> scheduleDTOList = new ArrayList<>();

        scheduleList.stream().forEach(schedule -> {
            //根据schedule的userId得到用户的类型和姓名
            Integer id = schedule.getUserId();
            Integer type = UserUtils.getUserTypeById(id);
            String name = UserUtils.getNameById(id);
            String aType = null;
            if (type == 1) {
                aType = "初访员";
            } else if (type == 3) {
                aType = "咨询师";
            }

            ScheduleDTO scheduleDTO = new ScheduleDTO();
            scheduleDTO.setPeriodNumber(schedule.getPeriodNumber() % 4);
            scheduleDTO.setName(name);
            scheduleDTO.setType(aType);
            scheduleDTO.setWeekDay( ((schedule.getPeriodNumber() - 1 ) / 4)+1 );
            scheduleDTOList.add(scheduleDTO);
        });

        return scheduleDTOList;
    }

}
