package com.aqoo.service.impl;

import com.aqoo.dto.RecordDTO;
import com.aqoo.entity.Consulting;
import com.aqoo.entity.Record;
import com.aqoo.mapper.RecordMapper;
import com.aqoo.service.ConsultingService;
import com.aqoo.service.RecordService;
import com.aqoo.utils.UserUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Constructor;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
@Service
public class RecordServiceImpl extends ServiceImpl<RecordMapper, Record> implements RecordService {

    @Autowired
    private ConsultingService consultingService;

    @Override
    public List<RecordDTO> getRecord(Integer applicationId) {

        LambdaQueryWrapper<Record> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Record::getApplicationId, applicationId).isNotNull(Record::getConsultRecord).orderByAsc(Record::getCreateTime);
        List<Record> list = list(wrapper);
        List<RecordDTO> recordDTOList = new ArrayList<>();

        list.stream().forEach(record -> {

            RecordDTO recordDTO = new RecordDTO();
            recordDTO.setStudentName(UserUtils.getNameById(record.getStudentId()))
                    .setConsultantName(UserUtils.getNameById(record.getConsultantId()))
                    .setResultRecord(record.getConsultRecord())
                    .setConsultDate(record.getConsultDate());

            String stateStr = null;
            //1-完成咨询 2-旷约 3-请假 4-脱落 5-结案
            Integer state = record.getState();
            switch (state) {
                case 1:
                    stateStr = "完成咨询";
                    break;
                case 2:
                    stateStr = "旷约";
                    break;
                case 3:
                    stateStr = "请假";
                    break;
                case 4:
                    stateStr = "脱落";
                    break;
                case 5:
                    stateStr = "结案";
                    break;
            }

            recordDTO.setState(stateStr);
            recordDTOList.add(recordDTO);
        });

        return recordDTOList;
    }

    @Override
    public void saveNextRecord(Record record) throws ParseException {
        Record nextRecord = new Record();
        nextRecord.setStudentId(record.getStudentId()).setApplicationId(record.getApplicationId())
                .setConsultantId(record.getConsultantId());

        String strDate = record.getConsultDate();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        Date date = ft.parse(strDate);

        Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(date);
        rightNow.add(Calendar.WEEK_OF_MONTH,1);

        date = rightNow.getTime();
        strDate = ft.format(date);

        nextRecord.setConsultDate(strDate);
        save(nextRecord);

        //将count字段加1
        LambdaUpdateWrapper<Consulting> wrapper = new LambdaUpdateWrapper<>();
        wrapper.eq(Consulting::getApplicationId, record.getApplicationId()).setSql("count = count + 1");
        consultingService.update(wrapper);
    }
}
