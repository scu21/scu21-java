package com.aqoo.service.impl;

import com.aqoo.VO.ConsultVO;
import com.aqoo.dto.ConsultDTO;
import com.aqoo.entity.Record;
import com.aqoo.entity.*;
import com.aqoo.mapper.ConsultingMapper;
import com.aqoo.service.*;
import com.aqoo.utils.DateUtils;
import com.aqoo.utils.PeriodUtils;
import com.aqoo.utils.UserUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
@Service
public class ConsultingServiceImpl extends ServiceImpl<ConsultingMapper, Consulting> implements ConsultingService {

    @Autowired
    private PeriodService periodService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private VisitService visitService;

    @Autowired
    private UserService userService;

    @Autowired
    private RecordService recordService;

    @Override
    public void commit(ConsultVO consultVO) {


        //根据日期和时间段查出period_number
        Integer dayOfWeek = DateUtils.transferDateTo(consultVO.getFirstDate());
        LambdaQueryWrapper<Period> wrapper = new LambdaQueryWrapper<Period>();
        wrapper.eq(Period::getDayWeek, dayOfWeek).eq(Period::getTimePeriod, consultVO.getPeriod());
        Period period = periodService.getOne(wrapper);
        Integer periodNumber = period.getNumber();
        consultVO.setPeriodNumber(periodNumber);
        Consulting consulting = new Consulting();
        BeanUtils.copyProperties(consultVO, consulting);

        consulting.setCount(0);
        save(consulting);

        //提交了申请以后  咨询师的状态就应该设置为忙碌
        LambdaUpdateWrapper<Schedule> wrapper1 = new LambdaUpdateWrapper<>();
        wrapper1.eq(Schedule::getUserId, consulting.getConsultantId()).set(Schedule::getIsFree, 0);
        scheduleService.update(wrapper1);

        //提交了申请后，初访表的is_arranged字段设为1
        Integer applicationId = consulting.getApplicationId();
        Integer studentId = consulting.getStudentId();
        LambdaUpdateWrapper<Visit> wrapper2 = new LambdaUpdateWrapper<>();
        wrapper2.eq(Visit::getApplicationId, applicationId).eq(Visit::getStudentId, studentId)
                .set(Visit::getIsArranged, 1);
        visitService.update(wrapper2);

        //提交申请后，record生成一条记录
        Record record = new Record();
        record.setConsultantId(consulting.getConsultantId()).setConsultDate(consulting.getFirstDate())
                .setApplicationId(consulting.getApplicationId()).setStudentId(consulting.getStudentId());
        recordService.save(record);

    }

    @Override
    public List<ConsultDTO> getConsult(Integer userId) {
        //查用户表  看当前用户是学生还是咨询师还是中心管理员
        User u = userService.getById(userId);
        Integer userType = u.getUserType();

        LambdaQueryWrapper<Consulting> wrapper = new LambdaQueryWrapper<>();
        if (userType == 0) {
            //如果是学生 只查自己的
            wrapper.eq(Consulting::getStudentId, userId);
        } else if (userType == 3) {
            //如果是咨询师，只查咨询了自己的
            wrapper.eq(Consulting::getConsultantId, userId);
        }
        //如果是管理员，则全查
        List<Consulting> consultingList = list(wrapper);

        List<ConsultDTO> consultDTOList = new ArrayList<>();
        //将Consulting包装成ConsultDTO
        consultingList.stream().forEach(consult -> {
            ConsultDTO consultDTO = new ConsultDTO();
            BeanUtils.copyProperties(consult, consultDTO);
            //设置学生、初访师、心理助理等人的姓名等字段
            Integer studentId = consultDTO.getStudentId();
            Integer consultId = consultDTO.getConsultantId();
            Integer assistantId = consultDTO.getAssistantId();

            consultDTO.setStudentName(UserUtils.getNameById(studentId))
                    .setStudentNumber(UserUtils.getLoginNumberById(studentId))
                    .setConsultName(UserUtils.getNameById(consultId))
                    .setAssistantName(UserUtils.getNameById(assistantId))
                    .setPeriod(PeriodUtils.getPeriodByNumber(consultDTO.getPeriodNumber()));

            Integer dayOfWeek = DateUtils.transferDateTo(consult.getFirstDate());
            String strWeek = "";
            switch (dayOfWeek) {
                case 1:
                    strWeek = "周一";
                    break;
                case 2:
                    strWeek = "周二";
                    break;
                case 3:
                    strWeek = "周三";
                    break;
                case 4:
                    strWeek = "周四";
                    break;
                case 5:
                    strWeek = "周五";
                    break;
                case 6:
                    strWeek = "周六";
                    break;
                case 7:
                    strWeek = "周日";
                    break;
            }
            consultDTO.setDayOfWeek(strWeek);

            if (consultDTO.getLastDate() != null) {
                consultDTO.setIsEnd(1);
            } else {
                consultDTO.setIsEnd(0);
            }

            consultDTOList.add(consultDTO);
        });

        return consultDTOList;

    }

    @Override
    public List<ConsultDTO> getMyConsulting(Integer consultantId) {

        LambdaQueryWrapper<Consulting> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Consulting::getConsultantId, consultantId);
        List<Consulting> consultingList = list(wrapper);

        List<ConsultDTO> consultDTOList = new ArrayList<>();

        consultingList.stream().forEach(consulting -> {
            ConsultDTO consultDTO = new ConsultDTO();
            BeanUtils.copyProperties(consulting, consultDTO);
            consultDTO.setStudentNumber(UserUtils.getLoginNumberById(consultDTO.getStudentId()));
            consultDTO.setStudentName(UserUtils.getNameById(consultDTO.getStudentId()));
            consultDTO.setAssistantName(UserUtils.getNameById(consultDTO.getAssistantId()));
            consultDTO.setPeriod(PeriodUtils.getPeriodByNumber(consultDTO.getPeriodNumber()));

            Integer dayOfWeek = DateUtils.transferDateTo(consulting.getFirstDate());
            String strWeek = "";
            switch (dayOfWeek) {
                case 1:
                    strWeek = "周一";
                    break;
                case 2:
                    strWeek = "周二";
                    break;
                case 3:
                    strWeek = "周三";
                    break;
                case 4:
                    strWeek = "周四";
                    break;
                case 5:
                    strWeek = "周五";
                    break;
                case 6:
                    strWeek = "周六";
                    break;
                case 7:
                    strWeek = "周日";
                    break;
            }

            consultDTO.setDayOfWeek(strWeek);

            consultDTOList.add(consultDTO);

        });

        return consultDTOList;
    }

    @Override
    public void end(Consulting consulting) {

        //结案：填充相关字段
        updateById(consulting);

        //将改咨询师的该时段排班表置为free
        Consulting consulting1 = getById(consulting.getId());
        Integer periodNumber = consulting1.getPeriodNumber();
        Integer consultantId = consulting1.getConsultantId();
        LambdaUpdateWrapper<Schedule> wrapper = new LambdaUpdateWrapper<>();
        wrapper.eq(Schedule::getUserId, consultantId).eq(Schedule::getPeriodNumber, periodNumber)
                .set(Schedule::getIsFree, 1);
        scheduleService.update(wrapper);


        //需要填写上最后一次的时间
        LambdaQueryWrapper<Record> wrapper1 = new LambdaQueryWrapper<>();
        wrapper1.eq(Record::getApplicationId, consulting1.getApplicationId()).isNotNull(Record::getConsultRecord).orderByDesc(Record::getCreateTime).last("limit 1");
        Record record = recordService.getOne(wrapper1);
        consulting1.setLastDate(record.getConsultDate());
        updateById(consulting1);

    }
}
