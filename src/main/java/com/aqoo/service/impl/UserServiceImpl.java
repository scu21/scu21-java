package com.aqoo.service.impl;

import com.aqoo.entity.Application;
import com.aqoo.entity.Schedule;
import com.aqoo.entity.User;
import com.aqoo.mapper.UserMapper;
import com.aqoo.service.ApplicationService;
import com.aqoo.service.PeriodService;
import com.aqoo.service.ScheduleService;
import com.aqoo.service.UserService;
import com.aqoo.utils.DateUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Autowired
    private PeriodService periodService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private UserService userService;

    @Override
    public List<User> getFreeInterviewers(String date, String period) {

        //根据日期计算星期几
        int dayOfWeek = DateUtils.transferDateTo(date);
        //获取到时间段的number
        int periodNumber = periodService.getPeriodNumber(period, dayOfWeek);

        //首先获取到application表中被约好的interviewersId（申请表状态位0/2）
        List<Integer> states = new ArrayList<>();
        states.add(0);
        states.add(2);
        LambdaQueryWrapper<Application> wrapper1 = new LambdaQueryWrapper<>();
        wrapper1.eq(Application::getApplyDate, date).eq(Application::getPeriodNumber, periodNumber)
                .in(Application::getState, states);
        List<Application> applicationList = applicationService.list(wrapper1);
        List<Integer> interviewersIdList = applicationList.stream().map(Application::getInterviewerId).collect(Collectors.toList());


        //查询所有指定时间的不在上述列表的初访员
        LambdaQueryWrapper<Schedule> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Schedule::getPeriodNumber, periodNumber).notIn(interviewersIdList.size() != 0, Schedule::getUserId, interviewersIdList);
        List<Schedule> schedules = scheduleService.list(wrapper);
        List<Integer> userIds = schedules.stream().map(Schedule::getUserId).collect(Collectors.toList());
        List<User> interviewers = null;
        if (userIds.size() != 0) {
            interviewers = listByIds(userIds);
            interviewers = interviewers.stream().filter(i -> i.getUserType() == 1).collect(Collectors.toList());
        }
        return interviewers;
    }

    @Override
    public List<User> getFreeConsultant(String date, String period) {
        //根据日期计算星期几
        int dayOfWeek = DateUtils.transferDateTo(date);
        //获取到时间段的number
        int periodNumber = periodService.getPeriodNumber(period, dayOfWeek);

        //查到该时间所有的空闲用户id
        LambdaQueryWrapper<Schedule> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Schedule::getIsFree, 1).eq(Schedule::getPeriodNumber, periodNumber);
        List<Schedule> scheduleList = scheduleService.list(wrapper);
        List<Integer> ids = scheduleList.stream().map(Schedule::getUserId).collect(Collectors.toList());

        //查找id在ids中的所有空闲咨询师
        LambdaQueryWrapper<User> wrapper1 = new LambdaQueryWrapper<>();
        wrapper1.eq(User::getUserType, 3).in(User::getId, ids);
        List<User> freeConsultantList = userService.list(wrapper1);
        return freeConsultantList;
    }
}
