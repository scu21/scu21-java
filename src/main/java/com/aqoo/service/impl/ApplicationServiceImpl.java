package com.aqoo.service.impl;

import com.aqoo.dto.ApplicationDTO;
import com.aqoo.entity.*;
import com.aqoo.mapper.ApplicationMapper;
import com.aqoo.service.*;
import com.aqoo.utils.DateUtils;
import com.aqoo.utils.PeriodUtils;
import com.aqoo.utils.UserUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
@Service
public class ApplicationServiceImpl extends ServiceImpl<ApplicationMapper, Application> implements ApplicationService {

    @Autowired
    private PeriodService periodService;

    @Autowired
    private UserService userService;

    @Autowired
    private VisitService visitService;

    @Override
    public boolean commit(Integer applicationId, Integer studentId, String date, String period, Integer interviewerId) {

        int dayOfWeek = DateUtils.transferDateTo(date);
        //查出该日期对应时间段记录的time_period字段
        LambdaQueryWrapper<Period> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Period::getDayWeek, dayOfWeek).eq(Period::getTimePeriod, period);
        Period one = periodService.getOne(wrapper);
        //查出本次流程该学生的申请表记录
        LambdaQueryWrapper<Application> wrapper1 = new LambdaQueryWrapper<>();
        wrapper1.eq(Application::getId, applicationId).eq(Application::getStudentId, studentId);
        Application application = getOne(wrapper1);

        //如果application中的interviewerId 不为空，说明本次流程已经申请过了
        //不能再申请
        if (application.getInterviewerId() != null)
            return false;

        //设置申请表的初访员字段和period_time字段和初访预约时间字段apply_date
        application.setInterviewerId(interviewerId);
        application.setPeriodNumber(one.getNumber());
        application.setApplyDate(date);
        //存入
        updateById(application);

/*        //修改初访员的状态信息
        LambdaUpdateWrapper<Schedule> wrapper2 = new LambdaUpdateWrapper<>();
        wrapper2.eq(Schedule::getUserId, interviewerId).eq(Schedule::getPeriodNumber, one.getNumber());
        wrapper2.set(Schedule::getIsFree, 0);
        scheduleService.update(wrapper2);*/


        return true;
    }

    @Override
    public List<ApplicationDTO> getAll(Integer userId) {

        //查用户表  看当前用户是学生还是初访员还是中心管理员
        User u = userService.getById(userId);
        Integer userType = u.getUserType();

        //要返回的ApplicationDTO的list
        List<ApplicationDTO> applicationDTOList = new ArrayList<>();

        //先查出所有的已经提交申请表（除开填完问卷就创建的）
        LambdaQueryWrapper<Application> wrapper = new LambdaQueryWrapper<>();
        wrapper.isNotNull(Application::getInterviewerId);

        if (userType == 0) {
            //说明是学生
            //只需要查学号是自己的就行
            wrapper.eq(Application::getStudentId, userId);
        } else if (userType == 1) {
            //说明是初访员
            //查看初访员是自己的并且状态为已审核通过的
            wrapper.eq(Application::getState, 1).eq(Application::getInterviewerId, userId);
        }

        //中心管理员
        //所有的已提交的申请表都要看

        List<Application> applicationList = list(wrapper);

        applicationList.stream().forEach(application -> {
            ApplicationDTO applicationDTO = new ApplicationDTO();
            applicationDTO.setApplyDate(application.getApplyDate()).setScore(application.getScore())
                    .setStatus(application.getState()).setApplicationId(application.getId())
                    .setStudentId(application.getStudentId());

            //根据学生id查询学生信息
            applicationDTO.setStudentNumber(UserUtils.getLoginNumberById(application.getStudentId()))
                    .setStudentName(UserUtils.getNameById(application.getStudentId()));

            //根据初访员id查初访员姓名
            applicationDTO.setInterviewerName(UserUtils.getNameById(application.getInterviewerId()));

            //根据period_number查预约时间段
            applicationDTO.setPeriod(PeriodUtils.getPeriodByNumber(application.getPeriodNumber()));



            //获取初访状态
            LambdaQueryWrapper<Visit> wrapper4 = new LambdaQueryWrapper<>();
            wrapper4.eq(Visit::getApplicationId, application.getId()).eq(Visit::getStudentId, application.getStudentId());
            Visit visit = visitService.getOne(wrapper4);
            if (visit != null) {
                applicationDTO.setInterviewState(visit.getState());
            } else {
                applicationDTO.setInterviewState(0);
            }

            applicationDTOList.add(applicationDTO);

        });

        return applicationDTOList;
    }

    @Override
    public void updateState(Integer applicationId, Integer state) {

        LambdaQueryWrapper<Application> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Application::getId, applicationId);
        Application application = getOne(wrapper);
        application.setState(state);
        updateById(application);

        //审核状态的时候，如果审核通过 还要新增一条初访记录
        if (state == 1) {
            Visit visit = new Visit();
            visit.setApplicationId(applicationId).setStudentId(application.getStudentId())
                    .setVisitDate(application.getApplyDate()).setPeriodNumber(application.getPeriodNumber());
            visitService.save(visit);
        }

    }

//    @Override
//    public List<ApplicationDTO> getMy(Integer userId) {
//
//        //查用户表  看当前用户是学生还是初访员
//        User user1 = userService.getById(userId);
//
//        //如果是初访员查出所有已经审核通过的申请了该初访员自己的申请表
//        // 如果是学生查出所有自己的申请表
//        LambdaQueryWrapper<Application> wrapper = new LambdaQueryWrapper<>();
//        if (user1.getUserType() == 1) {
//            wrapper.eq(Application::getState, 1).eq(Application::getInterviewerId, userId);
//        }
//        if (user1.getUserType() == 0) {
//            wrapper.eq(Application::getStudentId, userId);
//        }
//        List<Application> applicationList = list(wrapper);
//
//        //将查到的信息封装成DTO
//        List<ApplicationDTO> applicationDTOList = new ArrayList<>();
//
//        if (applicationList.size() == 0) {
//            return null;
//        }
//
//        applicationList.stream().forEach(application -> {
//
//            ApplicationDTO applicationDTO = new ApplicationDTO();
//            applicationDTO.setApplyDate(application.getApplyDate()).setScore(application.getScore())
//                    .setApplicationId(application.getId()).setStudentId(application.getStudentId()).setStatus(application.getState());
//
//            //将学生信息存入
//            LambdaQueryWrapper<User> wrapper1 = new LambdaQueryWrapper<>();
//            wrapper1.eq(User::getId, application.getStudentId());
//            User user = userService.getOne(wrapper1);
//            applicationDTO.setStudentName(user.getUsername()).setStudentNumber(user.getLoginNumber());
//
//            //将时间段信息存入
//            LambdaQueryWrapper<Period> wrapper2 = new LambdaQueryWrapper<>();
//            wrapper2.eq(Period::getNumber, application.getPeriodNumber());
//            Period period = periodService.getOne(wrapper2);
//            applicationDTO.setPeriod(period.getTimePeriod());
//
//            applicationDTOList.add(applicationDTO);
//        });
//
//        return applicationDTOList;
//    }
}
