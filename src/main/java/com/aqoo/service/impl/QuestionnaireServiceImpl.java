package com.aqoo.service.impl;

import com.aqoo.entity.Questionnaire;
import com.aqoo.mapper.QuestionnaireMapper;
import com.aqoo.service.QuestionnaireService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
@Service
public class QuestionnaireServiceImpl extends ServiceImpl<QuestionnaireMapper, Questionnaire> implements QuestionnaireService {

}
