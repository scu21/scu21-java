package com.aqoo.service;

import com.aqoo.dto.VisitDTO;
import com.aqoo.entity.Visit;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
public interface VisitService extends IService<Visit> {

    void commit(Visit visit);

    List<VisitDTO> getAllVisit(Integer userId);
}
