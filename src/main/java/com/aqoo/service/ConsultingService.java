package com.aqoo.service;

import com.aqoo.VO.ConsultVO;
import com.aqoo.dto.ConsultDTO;
import com.aqoo.entity.Consulting;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
public interface ConsultingService extends IService<Consulting> {

    void commit(ConsultVO consultVO);

    List<ConsultDTO> getConsult(Integer userId);

    List<ConsultDTO> getMyConsulting(Integer consultantId);

    void end(Consulting consulting);
}
