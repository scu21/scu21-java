package com.aqoo.service;

import com.aqoo.entity.Answer;
import com.aqoo.entity.Questionnaire;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
public interface AnswerService extends IService<Answer> {

    Integer saveAnswerAndScore(List<Questionnaire> list, Integer userId);
}
