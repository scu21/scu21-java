package com.aqoo.service;

import com.aqoo.dto.ScheduleDTO;
import com.aqoo.entity.Schedule;
import com.aqoo.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
public interface ScheduleService extends IService<Schedule> {

    List<ScheduleDTO> getSchedule(Integer userId);
}
