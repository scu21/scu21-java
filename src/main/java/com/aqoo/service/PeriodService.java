package com.aqoo.service;

import com.aqoo.entity.Period;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
public interface PeriodService extends IService<Period> {

    int getPeriodNumber(String period, int dayOfWeek);
}
