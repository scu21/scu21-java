package com.aqoo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 咨询预约 + 结案报告
 * @author lpx
 * @since 2023-06-23
 */
@Data
@TableName("consulting")
@Accessors(chain = true)
public class Consulting implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 学生id
     */
    private Integer studentId;

    /**
     * 流程id
     */
    private Integer applicationId;

    /**
     * 首次咨询时间
     */
    private String firstDate;

    /**
     * 最后一次咨询时间
     */
    private String lastDate;

    /**
     * 时间段标识
     */
    private Integer periodNumber;

    /**
     * 咨询地点
     */
    private String consultPlace;

    /**
     * 咨询总结
     */
    private String summary;

    /**
     * 咨询总次数
     */
    private Integer count;

    /**
     * 自评
     */
    private String selfComment;

    /**
     * 问题描述
     */
    private String description;

    /**
     * 心理助理id
     */
    private Integer assistantId;

    /**
     * 咨询师id
     */
    private Integer consultantId;

    /**
     * 是否追加
     * 0-未追加 1-申请追加 2-追加审核通过 3-追加审核不通过
     */
    private Integer isAdd;

    private Integer isDeleted;

    private LocalDateTime createTime;

    private LocalDateTime modifyTime;
}
