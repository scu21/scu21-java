package com.aqoo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 问卷答案
 * @author lpx
 * @since 2023-06-23
 */
@Data
@Accessors(chain = true)
@TableName("answer")
public class Answer implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 问题的id
     */
    private Integer questionnaireId;

    /**
     * 问题的分数
     */
    private Integer questionScore;

    /**
     * 本次流程id
     */
    private Integer applicationId;

    /**
     * 学生id
     */
    private Integer studentId;

    /**
     * 是否高危
     */
    private Integer isDangerous;

}
