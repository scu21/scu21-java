package com.aqoo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 咨询记录
 * @author lpx
 * @since 2023-06-23
 */
@Data
@TableName("record")
@Accessors(chain = true)
public class Record implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 流程id
     */
    private Integer applicationId;

    /**
     * 学生id
     */
    private Integer studentId;

    /**
     * 咨询师id
     */
    private Integer consultantId;

    /**
     * 咨询记录
     */
    private String consultRecord;

    /**
     * 1-完成咨询 2-旷约 3-请假 4-脱落 5-结案
     */
    private Integer state;

    /**
     * 咨询日期
     */
    private String consultDate;

    private Integer isDeleted;

    private LocalDateTime createTime;

    private LocalDateTime modifyTime;

}
