package com.aqoo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 初访
 * @author lpx
 * @since 2023-06-23
 */
@Data
@TableName("visit")
@Accessors(chain = true)
public class Visit implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 学生id
     */
    private Integer studentId;

    /**
     * 流程id
     */
    private Integer applicationId;

    /**
     * 危机等级 0-轻度 1-中度  2-重度
     */
    private Integer crisisLevel;

    /**
     * 初访描述
     */
    private String description;

    /**
     * 初访结论 0-无需咨询 1-安排咨询  2-转介送诊
     */
    private Integer conclusion;

    /**
     * 0-未初访   1-已初访
     */
    private Integer state;

    /**
     * 初访时间
     */
    private String visitDate;

    /**
     * 时间段标识
     */
    private Integer periodNumber;

    /**
     * 心理助理是否安排
     */
    private Integer isArranged;

    /**
     * 初访员id
     */
    private Integer interviewerId;

    private Integer isDeleted;

    private LocalDateTime createTime;

    private LocalDateTime modifyTime;

}
