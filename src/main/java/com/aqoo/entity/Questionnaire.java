package com.aqoo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 问卷
 * @author lpx
 * @since 2023-06-23
 */
@Data
@TableName("questionnaire")
public class Questionnaire implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 问题分数
     */
    private Integer questionScore;

    /**
     * 是否高危 1代表高危  0代表普通
     */
    private Integer isDangerous;

    /**
     * 问题
     */
    private String questionText;

    private Integer isDeleted;

    private LocalDateTime createTime;

    private LocalDateTime modifyTime;

}
