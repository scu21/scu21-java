package com.aqoo.entity;

import com.aqoo.utils.excel.ExcelExport;
import com.aqoo.utils.excel.ExcelImport;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户
 * @author lpx
 * @since 2023-06-23
 */
@Data
@TableName("user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 姓名
     */
    @ExcelImport("姓名")
    @ExcelExport(value = "姓名", example = "(示例数据)张三")
    private String username;

    /**
     * 登录账号（学号/工号）
     */
    @ExcelImport("学号/工号")
    @ExcelExport(value = "学号/工号", example = "20201414")
    private String loginNumber;

    /**
     * 性别
     */
    @ExcelImport("性别")
    @ExcelExport(value = "性别", example = "男")
    private String gender;

    /**
     * 学院
     */
    @ExcelImport("学院")
    @ExcelExport(value = "学院", example = "计算机学院")
    private String department;

    /**
     * 电话
     */
    @ExcelImport("联系电话")
    @ExcelExport(value = "联系电话", example = "110")
    private String phoneNumber;

    /**
     * 密码
     */
    @ExcelImport("密码")
    private String password;

    /**
     * 用户类型（0-学生、1-初访员、2-心理助理、3-咨询师、4-中心管理员）
     */
    @ExcelImport(value = "用户类型(学生、初访员、心理助理、咨询师、中心管理员)", kv = "0-学生;1-初访员;2-心理助理;3-咨询师;4-中心管理员")
    @ExcelExport(value = "用户类型(学生、初访员、心理助理、咨询师、中心管理员)", example = "学生")
    private Integer userType;

    private Integer isDeleted;

    private LocalDateTime createTime;

    private LocalDateTime modifyTime;

}
