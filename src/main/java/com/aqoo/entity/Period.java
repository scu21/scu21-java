package com.aqoo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 时间段
 * @author lpx
 * @since 2023-06-23
 */
@Data
@TableName("period")
public class Period implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 时间段
     */
    private String timePeriod;

    /**
     * 周几
     */
    private Integer dayWeek;

    /**
     * 标号
     */
    private Integer number;

    private Integer isDeleted;

    private LocalDateTime createTime;

    private LocalDateTime modifyTime;

}
