package com.aqoo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 排班
 * @author lpx
 * @since 2023-06-23
 */
@Data
@TableName("schedule")
public class Schedule implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer userId;

    private Integer periodNumber;

    private Integer isFree;

    private Integer isDeleted;

    private LocalDateTime createTime;

    private LocalDateTime modifyTime;
}
