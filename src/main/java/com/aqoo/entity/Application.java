package com.aqoo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 申请
 * @author lpx
 * @since 2023-06-23
 */
@Data
@TableName("application")
@Accessors(chain = true)
public class Application implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 学生id
     */
    private Integer studentId;

    /**
     * 申请时间段的标号
     */
    private Integer periodNumber;

    /**
     * 本次流程的问卷得分
     */
    private Integer score;

    /**
     * 0-审核中，1-审核通过、2-审核不通过
     */
    private Integer state;

    /**
     * 0-不是高危学生
     * 1-是高危学生
     */
    private Integer isDangerous;

    /**
     * 初访员id
     */
    private Integer interviewerId;

    /**
     * 初访预约时间
     */
    private String applyDate;

    private Integer isDeleted;

    private LocalDateTime createTime;

    private LocalDateTime modifyTime;

}
