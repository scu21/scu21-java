package com.aqoo.mapper;

import com.aqoo.entity.Record;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
public interface RecordMapper extends BaseMapper<Record> {

}
