package com.aqoo.mapper;

import com.aqoo.entity.Application;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lpx
 * @since 2023-06-23
 */
public interface ApplicationMapper extends BaseMapper<Application> {

}
