package com.aqoo.group21;

import com.aqoo.entity.User;
import com.aqoo.utils.PeriodUtils;
import com.aqoo.utils.UserUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Group21ApplicationTests {

    @Test
    void contextLoads() {

        System.out.println(UserUtils.getNameById(1));
        System.out.println(UserUtils.getUserTypeById(1));
        System.out.println(UserUtils.getLoginNumberById(1));
    }

}
